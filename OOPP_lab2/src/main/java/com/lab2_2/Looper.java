package com.lab2_2;

import java.util.Scanner;

public class Looper {

    public static void main(String[] args) {

        int algorithmId;
        int loopType;
        int n;

        Scanner scanner = new Scanner(System.in);
        System.out.print("Input algorithm id: ");
        algorithmId = scanner.nextInt();
        System.out.print("Input loop type: ");
        loopType = scanner.nextInt();
        System.out.print("Input n: ");
        n = scanner.nextInt();

        calculateAlgorithm(algorithmId, loopType, n);
    }

    private static void calculateAlgorithm(int algorithmId, int loopType, int n) {

        if (algorithmId == 1) {
            calculateFibonacci(loopType, n);
        } else if (algorithmId == 2) {
            calculateFactorial(loopType, n);
        } else {
            System.out.println("Incorrect algorithm id.");
        }
    }

    private static void calculateFibonacci(int loopType, int n) {

        int n0 = 0;
        int n1 = 1;
        int n2;
        int i = 2;
        System.out.print(n0 + " " + n1 + " ");

        if (loopType == 1) {

            while (i <= n) {
                n2 = n0 + n1;
                System.out.print(n2 + " ");
                n0 = n1;
                n1 = n2;
                i++;
            }
            System.out.println();

        } else if (loopType == 2) {

            do {
                n2 = n0 + n1;
                System.out.print(n2 + " ");
                n0 = n1;
                n1 = n2;
                i++;
            } while (i <= n);
            System.out.println();

        } else if (loopType == 3) {

            for(; i <= n; i++){
                n2 = n0 + n1;
                System.out.print(n2 + " ");
                n0 = n1;
                n1 = n2;
            }
            System.out.println();

        } else {
            System.out.println("Incorrect loop type.");
        }
    }

    private static void calculateFactorial(int loopType, int n) {

        int result = 1;
        int i = 1;

        if (loopType == 1) {

            while (i <= n) {
                result *= i;
            }

        } else if (loopType == 2) {

            do {
                result *= i;
            } while (i <= n);

        } else if (loopType == 3) {

            for (; i <= n; i++) {
                result *= i;
            }

        } else {
            System.out.println("Incorrect loop type.");
            return;
        }

        System.out.println(result);
    }
}
