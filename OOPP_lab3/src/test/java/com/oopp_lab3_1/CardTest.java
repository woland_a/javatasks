package com.oopp_lab3_1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CardTest {
    Card cardAlex = new Card("Alex", 930);
    Card cardMike = new Card("Mike", 52);

    @Test
    void getAccountBalance() {
        assertEquals(930, cardAlex.getAccountBalance());
        assertEquals(52, cardMike.getAccountBalance());
    }

    @Test
    void getAccountBalanceWithExchange() {
        assertEquals(300, cardAlex.getAccountBalance(3.10));
        assertEquals(20, cardMike.getAccountBalance(2.60));
    }

    @Test
    void getFrom() {
        double b_cardMike = cardMike.getAccountBalance();
        double b_cardAlex = cardAlex.getAccountBalance();
        cardAlex.getFrom(cardMike, 2);
        assertEquals(b_cardAlex + 2, cardAlex.getAccountBalance());
        assertEquals(b_cardMike - 2, cardMike.getAccountBalance());
    }

    @Test
    void addBalance() {
        double b_cardMike = cardMike.getAccountBalance();
        cardMike.addBalance(20);
        assertEquals(b_cardMike + 20, cardMike.getAccountBalance());
    }

    @Test
    void withdraw() {
        double b_cardAlex = cardAlex.getAccountBalance();
        double res = cardAlex.withdraw(20);
        assertEquals(20, res);
        assertEquals(b_cardAlex - 20, cardAlex.getAccountBalance());
    }
}