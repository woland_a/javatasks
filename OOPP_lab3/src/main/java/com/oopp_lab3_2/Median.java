package com.oopp_lab3_2;

import java.util.Arrays;

public class Median {
    public static float median(int[] numbers) {
        Arrays.sort(numbers);
        float med;

        int sInd = numbers.length / 2;

        if (numbers.length % 2 == 0) {
            int fInd = sInd - 1;

            med = (float)(numbers[fInd] + numbers[sInd]) / 2;
        }
        else {
            med = numbers[sInd];
        }
        return med;
    }

    public static double median(double[] numbers) {
        Arrays.sort(numbers);
        double med;

        int sInd = numbers.length / 2;

        if (numbers.length % 2 == 0) {
            int fInd = sInd - 1;

            med = (numbers[fInd] + numbers[sInd]) / 2;
        }
        else {
            med = numbers[sInd];
        }
        return med;
    }
}
