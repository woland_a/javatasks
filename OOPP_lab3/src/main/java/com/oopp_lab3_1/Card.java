package com.oopp_lab3_1;

public class Card {
    private String holderName;
    private double accountBalance;

    public Card(String holderName) {
        this.holderName = holderName;
        this.accountBalance = 0;
    }

    public Card(String holderName, double accountBalance) {
        this.holderName = holderName;
        this.accountBalance = accountBalance;
    }

    public double getAccountBalance() {
        return accountBalance;
    }

    public double getAccountBalance(double exchangeRate) {
        return accountBalance / exchangeRate;
    }

    public void getFrom(Card senderCard, double moneyAmount) {
        this.accountBalance += senderCard.withdraw(moneyAmount);
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public void setAccountBalance(double accountBalance) {
        this.accountBalance = accountBalance;
    }

    public void addBalance(double moneyAmount) {
        this.accountBalance += moneyAmount;
    }

    public double withdraw(double moneyAmount) {
        if ( moneyAmount <= accountBalance) {
            this.accountBalance -= moneyAmount;
            return moneyAmount;
        } else {
            System.out.println("Not enough money to withdraw.");
            return 0;
        }
    }
}
